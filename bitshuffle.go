package bitshuffle

// #cgo CFLAGS: -g -Wall
// #include <stdlib.h>
// #include "bitshuffle.h"
import "C"

import (
	"fmt"
	"unsafe"
)

/* ---- Shuffle ----
 *
 * Bitshuffle the data.
 *
 * Wrapper for C binding for shuffling data.
 *
 * Parameters
 * ----------
 *  elementSize : element size of typed data (1 for uint8, 2 for uint16...)
 *  blockSize : Do transpose in blocks of this many elements. Pass 0 to
 *  select automatically (recommended).
 *
 * Returns
 * -------
 *  shuffled byte array, error if failed.
 *
 */

func Shuffle(data []byte, elementSize, blockSize int) ([]byte, error) {
	var buf []byte

	dataLength := len(data)
	dataPtr := unsafe.Pointer(&data[0])

	shuffledDataPtr := C.malloc(C.sizeof_char * C.ulong(dataLength))
	defer C.free(unsafe.Pointer(shuffledDataPtr))

	cSize := C.size_t(dataLength / elementSize)
	cElementSize := C.size_t(elementSize)
	cBlockSize := C.size_t(blockSize)

	i := C.bshuf_bitshuffle(dataPtr, shuffledDataPtr, cSize, cElementSize, cBlockSize)

	numberOfShuffledBytes := int(i)

	if numberOfShuffledBytes < 0 {
		return buf, fmt.Errorf("Error %d", numberOfShuffledBytes)
	}

	buf = C.GoBytes(shuffledDataPtr, C.int(i))

	if numberOfShuffledBytes != len(buf) {
		return buf, fmt.Errorf("Not all data could be shuffled (%d out of %d)", numberOfShuffledBytes, dataLength)
	}

	return buf, nil
}

/* ---- Unshuffle ----
 *
 * Unshuffle bitshuffled data.
 *
 * Wrapper for C binding for unshuffling data.
 *
 * To properly unshuffle bitshuffled data, *elementSize* and *blockSize*
 * must match the parameters used to shuffle the data.
 *
 * Parameters
 * ----------
 *  elementSize : element size of typed data (1 for uint8, 2 for uint16...)
 *  blockSize : Do transpose in blocks of this many elements. Pass 0 to
 *  select automatically (recommended).
 *
 * Returns
 * -------
 *  unshuffled byte array, error if failed.
 *
 */

func Unshuffle(shuffledData []byte, elementSize, blockSize int) ([]byte, error) {
	var buf []byte

	dataLength := len(shuffledData)
	dataPtr := unsafe.Pointer(&shuffledData[0])

	unshuffledDataPtr := C.malloc(C.sizeof_char * C.ulong(dataLength))
	defer C.free(unsafe.Pointer(unshuffledDataPtr))

	cSize := C.size_t(dataLength / elementSize)
	cElementSize := C.size_t(elementSize)
	cBlockSize := C.size_t(blockSize)

	i := C.bshuf_bitunshuffle(dataPtr, unshuffledDataPtr, cSize, cElementSize, cBlockSize)

	numberOfUnshuffledBytes := int(i)

	if numberOfUnshuffledBytes < 0 {
		return buf, fmt.Errorf("Error %d", numberOfUnshuffledBytes)
	}

	buf = C.GoBytes(unshuffledDataPtr, C.int(i))

	if numberOfUnshuffledBytes != len(buf) {
		return buf, fmt.Errorf("Not all data could be unshuffled (%d out of %d)", numberOfUnshuffledBytes, dataLength)
	}

	return buf, nil
}
