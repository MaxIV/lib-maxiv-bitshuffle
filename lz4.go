package bitshuffle

// #cgo CFLAGS: -g -Wall
// #include <stdlib.h>
// #include "bitshuffle.h"
import "C"

import (
	"fmt"
	"unsafe"
)

/* ---- bshuf_compress_lz4 ----
 *
 * Bitshuffled and compress the data using LZ4.
 *
 * Transpose within elements, in blocks of data of *block_size* elements then
 * compress the blocks using LZ4.  In the output buffer, each block is prefixed
 * by a 4 byte integer giving the compressed size of that block.
 *
 * Output buffer must be large enough to hold the compressed data.  This could
 * be in principle substantially larger than the input buffer.  Use the routine
 * *bshuf_compress_lz4_bound* to get an upper limit.
 *
 * Parameters
 * ----------
 *  in : input buffer, must be of size * elem_size bytes
 *  out : output buffer, must be large enough to hold data.
 *  size : number of elements in input
 *  elem_size : element size of typed data
 *  block_size : Process in blocks of this many elements. Pass 0 to
 *  select automatically (recommended).
 *
 * Returns
 * -------
 *  number of bytes used in output buffer, negative error-code if failed.
 *
 */
func CompressLZ4(data []byte, elementSize, blockSize int) ([]byte, error) {
	var buf []byte

	dataLength := len(data)
	dataPtr := unsafe.Pointer(&data[0])

	compressedDataPtr := C.malloc(C.sizeof_char * C.ulong(dataLength))
	defer C.free(unsafe.Pointer(compressedDataPtr))

	cSize := C.size_t(dataLength / elementSize)
	cElementSize := C.size_t(elementSize)
	cBlockSize := C.size_t(blockSize)

	i := C.bshuf_compress_lz4(dataPtr, compressedDataPtr, cSize, cElementSize, cBlockSize)

	numberOfCompressedBytes := int(i)

	if numberOfCompressedBytes < 0 {
		return buf, fmt.Errorf("Error %d", numberOfCompressedBytes)
	}

	buf = C.GoBytes(compressedDataPtr, C.int(i))

	if numberOfCompressedBytes != len(buf) {
		return buf, fmt.Errorf("Not all data could be shuffled (%d out of %d)", numberOfCompressedBytes, dataLength)
	}

	return buf, nil
}
